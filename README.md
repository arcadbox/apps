# Apps

[ArcadBox](https://gitlab.com/arcadbox) integrated apps.

- [Hextris](./hextris)
- [2048](./2048)

## Bundling apps

```
make clean
make bundle
```

Apps bundles will copied in the `./bundles` directory.