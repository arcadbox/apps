import { all } from 'redux-saga/effects';
import { arcadSaga } from './arcad';

export default function* rootSaga() {
  yield all([
    arcadSaga(),
  ]);
}
