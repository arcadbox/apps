import { eventChannel, END } from 'redux-saga';
import { call, take, put, takeLatest, all } from 'redux-saga/effects';
import { backendEvent } from '../actions/backend';
import { SELECT_ANSWER_REQUEST, selectAnswerFailure, selectAnswerSuccess } from '../actions/answer';

const Arcad = window.Arcad;

export function* arcadSaga() {
  yield all([
    handleArcadBackendEventSaga(),
    takeLatest(SELECT_ANSWER_REQUEST, sendSelectedAnswerSaga),
  ]);
}

export function* handleArcadBackendEventSaga() {
  yield put({ type: 'BACKEND_CHANNEL_START' });
  const chan = yield call(arcadChannel, "quiz")
  try {    
    while (true) {
      let { type, ...attrs } = yield take(chan);
      yield put(backendEvent(type, attrs));
    }
  } catch(err) {
    chan.close();
    yield put({ type: 'BACKEND_CHANNEL_ERROR', error: err });
  } finally {
    console.log('arcad connection lost')
  }
  yield put({ type: 'BACKEND_CHANNEL_STOP' });
}

function arcadChannel(gameId) {
  return eventChannel(emitter => {
      Arcad.connect(gameId)
        .catch(err => {
          emitter(err);
          emitter(END);
        });

      const onEvent = evt => {
        emitter(evt.detail);
      };
      const onConnect = () => {
        emitter({ type: 'CHANNEL_CONNECT' });
      };
      const onDisconnect = () => {
        emitter({ type: 'CHANNEL_DISCONNECT' });
      };
      const onError = (err) => {
        emitter(err);
      };

      Arcad.addEventListener('event', onEvent);
      Arcad.addEventListener('connect', onConnect);
      Arcad.addEventListener('disconnect', onDisconnect);
      Arcad.addEventListener('error', onError);

      return () => {
        Arcad.removeEventListener('event', onEvent);
        Arcad.removeEventListener('connect', onConnect);
        Arcad.removeEventListener('disconnect', onDisconnect);
        Arcad.removeEventListener('error', onError);
        Arcad.disconnect();
      }
    }
  )
}

function* sendSelectedAnswerSaga({ questionId, selectedAnswerIndex }) {
  try {
    yield call(Arcad.invoke, 'selectAnswer', { questionId, selectedAnswerIndex });
  } catch(err) {
    console.error(err);
    put(selectAnswerFailure(err, questionId, selectedAnswerIndex));
    return;
  }
  yield put(selectAnswerSuccess(questionId, selectedAnswerIndex));
}