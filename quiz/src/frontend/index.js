import ReactDOM from 'react-dom'
import { ConnectedApp as App } from './app'
import { configureStore } from './store/store'
import { Provider } from 'react-redux'

import 'onsenui/css/onsenui.css';
import 'onsenui/css/onsen-css-components.css';

// import 'bulma/css/bulma.css';
import './css/base.css';
// import './css/view.css';
import './css/progress-ring.css';
import './css/timer.css';
import './css/progress-bar.css';

const store = configureStore()

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>, 
  document.getElementById('app')
)
