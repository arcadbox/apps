import React, { useState } from 'react';
import { Page, Card } from 'react-onsenui';
import { useDispatch, shallowEqual, useSelector } from 'react-redux';
import { QuestionInfo } from '../components/question-info';
import { Countdown, LinearMode } from '../components/countdown';

export function AnswerPage({ title }) {
  const { question, theme, timeout, delay, player } = useSelector(({ questions, views, players }) => {
    return {
      theme: questions.theme,
      question: questions.current,
      timeout: views.timeout,
      delay: views.delay,
      player: players.scores[players.current.id],
    };
  }, shallowEqual);

  if (!question) return null;

  const solution = question.answers[question.solutionIndex];

  let message;
  let messageClass;
  let didWin = false;
  if (question.selectedAnswerIndex === null) {
    message = "Vous donnez votre langue au chat ?";
    messageClass = "";
  } else if (question.solutionIndex === question.selectedAnswerIndex) {
    message = "Bonne réponse !";
    messageClass = "has-text-success";
    didWin = true;
  } else {
    message = "Perdu !";
    messageClass = "has-text-danger";
  }

  const scoreDelta = player.score - player.prevScore;

  return (
    <Page title={title}>
      <Countdown timeout={timeout} delay={delay} mode={LinearMode} />
      <QuestionInfo theme={theme} difficultyLevel={question.difficultyLevel} />
      <Card>
        <h4>{ message }</h4>
        {
          !didWin ?
          <p>La bonne réponse était "<b>{solution}</b>".</p> :
          null
        }
        {
          player.wasFirst ?
          <p>Félicitation, vous avez été le premier à trouver la bonne réponse !</p> :
          null
        }
        {
          scoreDelta > 0 ?
          <p>Vous gagnez <b>{scoreDelta} points.</b></p> :
          null
        }
        {
          scoreDelta < 0 ?
          <p>Vous perdez <b>{Math.abs(scoreDelta)} points.</b></p> :
          null
        }
        <p>Vous avez un total de <b>{player.score}</b> point(s).</p>
      </Card>
      <Card>
        <h5>Le saviez vous ?</h5>
        <p>{ question.trivia }</p>
      </Card>
    </Page>
  );
}