import React from 'react';
import { Page, ListTitle, List, ListItem, Card } from 'react-onsenui';
import { shallowEqual, useSelector } from 'react-redux';
import { Countdown, LinearMode } from '../components/countdown';

export function LeaderboardPage({ title }) {
  const { leaderboard, timeout, delay } = useSelector(({ views, players }) => {
    return {
      leaderboard: players.leaderboard,
      timeout: views.timeout,
      delay: views.delay,
    };
  }, shallowEqual);
  
  return (
    <Page title={title}>
      <Countdown timeout={timeout} delay={delay} mode={LinearMode} />
      <ListTitle>Classement général</ListTitle>
      <List
        dataSource={leaderboard}
        renderRow={(player, idx) => {
          return (
            <ListItem key={`player-${idx}`}>
              <div className="left">{idx+1}</div>
              <div className="center">{player.nickname}</div>
              <div className="right">{player.score}pts</div>
            </ListItem>
          );
        }}
      />
      <div className="is-padded">
        <p>
          Ce classement affiche uniquement les scores des joueurs connectés.
          Voir le tableau des scores pour les records.
        </p>
      </div>
    </Page>
  );
}