import React, { useState } from 'react';
import { Page, Card, Tabbar, Tab, Button } from 'react-onsenui';
import { useDispatch, shallowEqual, useSelector } from 'react-redux';
import { startGame } from '../actions/game';
import { Countdown, LinearMode } from '../components/countdown';
import { QuestionInfo } from '../components/question-info';

export function HomePage({ title }) {
  const [ tabIndex, setTabIndex ] = useState(0);
  return (
    <Page title={title}>
      <Tabbar
        onPreChange={({index}) => setTabIndex(index)}
        position='bottom'
        index={tabIndex}
        renderTabs={(activeIndex, tabbar) => [
          {
            content: <LaunchPage key="launch-page" title="Lancer le jeu" active={activeIndex === 0} tabbar={tabbar} />,
            tab: <Tab key="launch-tab" label="Accueil" icon="fa-home" />
          },
          {
            content: <RulesPage key="rules-page" title="Règles du jeu" active={activeIndex === 1} tabbar={tabbar} />,
            tab: <Tab key="rules-tab" label="Règles du jeu" icon="fa-question-circle" />
          }]
        }
      />
    </Page>
  )
}

export function LaunchPage() {
  
  const { connected, gameStarted, delay, timeout } = useSelector(({ views }) => {
    return {
      connected: views.connected,
      gameStarted: views.gameStarted,
      delay: views.delay,
      timeout: views.timeout,
    };
  }, shallowEqual);

  const dispatch = useDispatch();
  const onStartGameClick = () => dispatch(startGame());

  return (
    <Page>
      <Card>
        <h3>Bienvenue sur Quiz !</h3>
        <p>
          Affrontez les autres joueurs dans une série de question de culture générale, allant
          des Héros Marvel à l'histoire de France en passant par les grandes inventions techniques
          des siècles derniers !
        </p>
      </Card>
      <div className="is-padded">
        <Button modifier="large--cta"
          disabled={!connected || gameStarted}
          onClick={onStartGameClick}>
          {
            gameStarted ?
            "En attente de la prochaine question..." :
            "Rejoindre la partie"
          }
        </Button>
        {
          gameStarted ?
          <Countdown mode={LinearMode} delay={delay} timeout={timeout} /> :
          null
        }
      </div>
    </Page>
  );
}

export function RulesPage() {
  return (
    <Page>
      <Card>
        <h3>Règles du jeu</h3>
        <p>Tous les joueurs connectés reçoivent en même temps la même question.</p>
        <p><b>Une bonne réponse rapporte <span>10 points.</span></b> Si vous êtes le plus rapide à répondre, ces points sont doublés !</p>
        <p><b>Mauvaise réponse ? Vous perdez <span>5 points.</span></b></p>
        <p><b>Pas de réponse ? Pas de point gagné.</b></p>
        <p>La partie ne s'arrête jamais ! Votre score est calculé au moment où vous quittez le jeu.</p>
        <h5>Arriverez vous à battre les meilleurs joueurs ?</h5>
      </Card>
    </Page>
  );
}