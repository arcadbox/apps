import React, { useState } from 'react';
import { Page, ListTitle, List, ListItem, Radio } from 'react-onsenui';
import { useDispatch, shallowEqual, useSelector } from 'react-redux';
import { QuestionInfo } from '../components/question-info';
import { Countdown } from '../components/countdown';
import { selectAnswer } from '../actions/answer';

export function QuestionPage({ title }) {
  const dispatch = useDispatch();
  const [ selectedAnswerIndex, setSelectedAnswerIndex ] = useState(null);
  const handleAnswer = (answerIndex) => {
    if (answerIndex === selectedAnswerIndex) return;
    setSelectedAnswerIndex(answerIndex);
    dispatch(selectAnswer(question.id, answerIndex));
  };

  const { question, theme, timeout, delay } = useSelector(({ questions, views }) => {
    return {
      question: questions.current,
      theme: questions.theme,
      timeout: views.timeout,
      delay: views.delay,
    };
  }, shallowEqual);

  if (!question) return null;

  return (
    <Page title={title}>
      <QuestionInfo theme={theme} difficultyLevel={question.difficultyLevel} />
      <Countdown timeout={timeout} delay={delay} />
      <div className="is-padded">
        <h4>{ question.statement }</h4>
      </div>
      <List
        dataSource={question.answers}
        renderRow={(answer, idx) => {
          return (
            <ListItem
              tappable={selectedAnswerIndex !== idx}
              onClick={handleAnswer.bind(null, idx)}
              key={`answer-${idx}`}>
              <div className="left">
                {answer}
              </div>
              <div className="center"></div>
              <div className="right">
                <Radio 
                  checked={selectedAnswerIndex === idx} />
              </div>
            </ListItem>
          );
        }}
      />
      <p className="is-small" style={{textAlign: 'center'}}>
        <em>Une question de {question.author}</em>
      </p>
    </Page>
  );
}