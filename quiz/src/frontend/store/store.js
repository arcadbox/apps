import { createStore, applyMiddleware, combineReducers, compose } from 'redux'
import createSagaMiddleware from 'redux-saga'
import rootSaga from '../sagas/root'
import rootReducer from '../reducers/root';

const sagaMiddleware = createSagaMiddleware()

const combinedReducer = combineReducers(rootReducer);

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export function configureStore(initialState = {}) {
    const store = createStore(
        combinedReducer,
        initialState,
        composeEnhancers(
          applyMiddleware(sagaMiddleware)
        )
    )
    sagaMiddleware.run(rootSaga);
    return store;
}
