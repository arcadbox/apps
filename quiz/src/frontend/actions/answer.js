export const SELECT_ANSWER_REQUEST = 'SELECT_ANSWER_REQUEST';
export const SELECT_ANSWER_SUCCESS = 'SELECT_ANSWER_SUCCESS';
export const SELECT_ANSWER_FAILURE = 'SELECT_ANSWER_FAILURE';

export function selectAnswer(questionId, selectedAnswerIndex) {
  return { type: SELECT_ANSWER_REQUEST, questionId, selectedAnswerIndex };
};

export function selectAnswerSuccess(questionId, selectedAnswerIndex) {
  return { type: SELECT_ANSWER_SUCCESS, questionId, selectedAnswerIndex };
};

export function selectAnswerFailure(error, questionId, selectedAnswerIndex) {
  return { type: SELECT_ANSWER_FAILURE, error, questionId, selectedAnswerIndex };
};