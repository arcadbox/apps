import React, { Fragment, useState, useEffect, useRef } from 'react';
import { connect } from 'react-redux';
import { Navigator } from 'react-onsenui';
import { HomePage } from './pages/HomePage';
import { QuestionPage } from './pages/QuestionPage';
import { AnswerPage } from './pages/AnswerPage';
import { LeaderboardPage } from './pages/LeaderboardPage';

const routes = {
  home: {
    modelId: 'home',
    title: "Home",
    render: route => <HomePage title={route.title} />
  },
  question: {
    modelId: 'question',
    title: "Question",
    render: route => <QuestionPage title={route.title} />
  },
  answer: {
    modelId: 'answer',
    title: "Answer",
    render: route => <AnswerPage title={route.title} />
  },
  leaderboard: {
    modelId: 'leaderboard',
    title: "Leaderboard",
    render: route => <LeaderboardPage title={route.title} />
  },
}

export function App({ currentState, connected, gameStarted }) {
  const navigator = useRef(null);
  const [ prevState, setPrevState ] = useState(null);
  const [ prevRoute, setPrevRoute ] = useState(routes.home);
  const [ routeIndex, setRouteIndex ] = useState(0);
  
  useEffect(() => {
    if (!navigator.current) return;

    let routeModel;
    if (!connected || !gameStarted) {
      routeModel = routes.home;
    } else if (currentState === 'QUESTION_STATE') {
      routeModel = routes.question;
    } else if (currentState === 'ANSWER_STATE' && prevState === 'QUESTION_STATE') {
      routeModel = routes.answer;
    } else if (currentState === 'LEADERBOARD_STATE' && prevState === 'ANSWER_STATE') {
      routeModel = routes.leaderboard;
    }

    if (routeModel && prevRoute.modelId !== routeModel.modelId) {
      setRouteIndex((routeIndex) => {
        routeIndex = routeIndex+1
        let newRoute = {...routeModel, id: `${routeModel.modelId}-${routeIndex}`};
        console.log(`opening route "${newRoute.id}"`);
        navigator.current.resetPage(newRoute);
        setPrevRoute(newRoute);
        return routeIndex;
      });
    }
   
    setPrevState(currentState);
  }, [currentState]);

  return (
    <Navigator ref={navigator}
      renderPage={(route, navigator) => {
        return (
          <Fragment key={`nav-route-${route.id}`}>
            { route.render(route, navigator) }
          </Fragment>
        );
      }}
      animation="slide"
      initialRoute={routes.home}
    />
  );
}

function mapStateToProps({ views }) {
  return {
    currentState: views.currentState,
    connected: views.connected,
    gameStarted: views.gameStarted,
  };
}

export const ConnectedApp = connect(mapStateToProps)(App);