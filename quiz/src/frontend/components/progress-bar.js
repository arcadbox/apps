export function ProgressBar({ progress  }) {
  
  return (
    <progress className="progress-bar progress is-info is-small" 
      value={Number.isNaN(progress) ? null : progress}  max="100" />
  );
}