export function ProgressRing({ radius, stroke, progress, remaining  }) {
  const normalizedRadius = radius - stroke * 2;
  const circumference = normalizedRadius * 2 * Math.PI;
  const strokeDashoffset = circumference - (progress / 100 * circumference);

  let color;
  if (progress < 25) {
    color = '#49c774';
  } else if (progress < 50) {
    color = '#3273dc';
  } else if (progress < 75) {
    color = '#ffdd57';
  } else {
    color = '#ff3860';
  }

  return (
    <svg className="progress-ring"
      height={radius * 2}
      width={radius * 2}>
      <circle
        stroke={color}
        fill="transparent"
        strokeWidth={ stroke }
        strokeDasharray={ `${circumference} ${circumference}` }
        style={ { strokeDashoffset } }
        strokeWidth={ stroke }
        r={ normalizedRadius }
        cx={ radius }
        cy={ radius }
      />
      <text x="50%" y="50%" 
        dominantBaseline="central" 
        textAnchor="middle">
        {remaining}
      </text>
    </svg>
  );
}