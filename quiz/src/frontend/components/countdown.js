import { useEffect, useState } from 'react';
import { ProgressRing } from './progress-ring';
import { ProgressBar } from './progress-bar';

export const LinearMode = 'linear';
export const CircularMode = 'circular';

export function Countdown({ timeout, delay, mode = CircularMode }) {

  const getDelay = () => timeout ? timeout.getTime() - Date.now() : delay;
  const [ currentDelay, setCurrentDelay ] = useState(delay);

  const remaining = Math.ceil(currentDelay/1000);
  const progress = ((delay-currentDelay)/delay)*100;

  useEffect(() => {
    const timeoutId = setTimeout(() => {
      const currentDelay = getDelay();
      setCurrentDelay(currentDelay < 0 ? 0 : currentDelay);
    }, 1000);
    return () => {
      clearTimeout(timeoutId);
    }
  });

  return (
    <div className="timer">
      {
        mode === CircularMode ?
        <ProgressRing stroke={4} radius={40} progress={progress} remaining={remaining} /> :
        <ProgressBar progress={100-progress} />
      }
    </div>
  );
}