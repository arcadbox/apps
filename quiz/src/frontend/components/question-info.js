import { DifficultyLevel } from "./difficulty-level";

export function QuestionInfo({ theme, difficultyLevel }) {
  return (
    <div className="is-padded tags" style={{marginBottom:"1em"}}>
      <span className="tag" >{theme}</span>
      <DifficultyLevel difficultyLevel={difficultyLevel} />
    </div>
  );
}