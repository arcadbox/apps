import { DifficultyLevelBeginner, DifficultyLevelIntermediate, DifficultyLevelExpert } from "../../backend/game/quiz/quiz";

export function DifficultyLevel({ difficultyLevel }) {
  let difficultyLabel;
  let difficultyClass;

  switch (difficultyLevel) {
    case DifficultyLevelBeginner:
      difficultyLabel = "Débutant";
      difficultyClass = "is-success"
      break;
    case DifficultyLevelIntermediate:
      difficultyLabel = "Intermédiaire";
      difficultyClass = "is-warning"
      break;
    case DifficultyLevelExpert:
      difficultyLabel = "Expert";
      difficultyClass = "is-danger"
      break;
    default:
      return null;
  }

  return (
    <span className={`tag is-size-6 is-light ${difficultyClass}`}>{difficultyLabel}</span>
  )
}