import { BACKEND_NEW_QUESTION, BACKEND_NEW_THEME, BACKEND_NEW_ANSWER, BACKEND_CHANNEL_DISCONNECT } from "../actions/backend";
import { SELECT_ANSWER_REQUEST } from "../actions/answer";

const initialState = {
  theme: "",
  current: {
    id: null,
    question: "",
    author: "",
    answers: [],
    solution: null,
    anecdote: null,
    selectedAnswerIndex: null,
    difficultyLevel: null,
  },
};

export function questionsReducer(state = initialState, action) {
  switch (action.type) {
    case BACKEND_NEW_QUESTION:
      return handleNewQuestion(state, action);
    case BACKEND_NEW_ANSWER:
        return handleNewAnswer(state, action);
    case BACKEND_NEW_THEME:
      return handleNewTheme(state, action);
    case SELECT_ANSWER_REQUEST:
      return handleSelectAnswer(state, action);
    case BACKEND_CHANNEL_DISCONNECT:
      return handleBackendChannelDisconnect(state, action);
  }
  return state;
}

function handleNewTheme(state, { theme }) {
  return {
    ...state,
    theme,
  }
}

function handleNewQuestion(state, { statement, answers, theme, author, difficultyLevel }) {
  return {
    ...state,
    theme: theme,
    author: author,
    current: {
      ...state.current,
      statement,
      difficultyLevel,
      answers: [ ...answers ],
      solutionIndex: null,
      trivia: null,
      selectedAnswerIndex: null,
    },
  }
}

function handleSelectAnswer(state, { selectedAnswerIndex }) {
  return {
    ...state,
    current: {
      ...state.current,
      selectedAnswerIndex,
    },
  }
}

function handleNewAnswer(state, { theme, author, statement, answers, solutionIndex, trivia }) {
  return {
    ...state,
    theme: theme,
    current: {
      ...state.current,
      author,
      statement,
      answers: [ ...answers ],
      solutionIndex,
      trivia
    },
  }
}

function handleBackendChannelDisconnect(state, action) {
  return {
    ...state,
    theme: "",
    current: {
      ...state.current,
      id: null,
      question: "",
      author: "",
      answers: [],
      solution: null,
      anecdote: null,
      selectedAnswerIndex: null,
      difficultyLevel: null,
    },
  };
}