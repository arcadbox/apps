import { BACKEND_NEW_STATE, BACKEND_CHANNEL_DISCONNECT, BACKEND_CHANNEL_CONNECT } from "../actions/backend";
import { START_GAME } from "../actions/game";

const initialState = {
  prevState: null,
  currentState: null,
  timeout: null,
  delay: null,
  connected: false,
  gameStarted: false,
};

export function viewsReducer(state = initialState, action) {
  switch(action.type) {
    case BACKEND_NEW_STATE:
      return handleNewState(state, action);
    case BACKEND_CHANNEL_DISCONNECT:
      return handleBackendChannelDisconnect(state, action);
    case BACKEND_CHANNEL_CONNECT:
      return handleBackendChannelConnect(state, action);
    case START_GAME:
      return handleStartGame(state, action);
  }
  return state;
}

function handleNewState(state, { newState, delay, prevState }) {
  if (!state.gameStarted) return state;
  return {
    ...state,
    delay,
    prevState,
    currentState: newState,
    timeout: new Date(Date.now() + delay),
  };
}

function handleBackendChannelDisconnect(state) {
  return {
    ...state,
    connected: false,
  };
}

function handleBackendChannelConnect(state) {
  return {
    ...state,
    connected: true,
  };
}

function handleStartGame(state) {
  return {
    ...state,
    gameStarted: true,
  };
}