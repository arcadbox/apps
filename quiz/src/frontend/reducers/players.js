import { BACKEND_NEW_ANSWER, BACKEND_PLAYER_INFO, BACKEND_NEW_LEADERBOARD } from "../actions/backend";

const initialState = {
  current: {
    id: null,
    nickname: "",
  },
  scores: {},
  leaderboard: [],
};

export function playersReducer(state = initialState, action) {
  switch(action.type) {
    case BACKEND_NEW_ANSWER:
      return handleNewAnswer(state, action);
    case BACKEND_PLAYER_INFO:
      return handlePlayerInfo(state, action);
    case BACKEND_NEW_LEADERBOARD:
      return handleNewLeaderboard(state, action);
  }
  return state;
}

function handleNewAnswer(state, { results }) {
  return {
    ...state,
    scores: {
      ...state.scores,
      ...Object.keys(results).reduce((extendResults, playerId) => {
        extendResults[playerId] = {
          ...results[playerId],
          prevScore: state.scores[playerId] ? state.scores[playerId].score : 0,
        };
        return extendResults;
      }, {}),
    }
  };
}

function handlePlayerInfo(state, { player: { id, nickname } }) {
  return {
    ...state,
    current: {
      ...state.current,
      id, nickname,
    }
  };
}

function handleNewLeaderboard(state, { leaderboard }) {
  return {
    ...state,
    leaderboard: [ ...leaderboard ],
  };
}