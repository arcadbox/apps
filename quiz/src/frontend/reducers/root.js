import { viewsReducer } from "./views";
import { questionsReducer } from "./questions";
import { playersReducer } from "./players";

export default {
  views: viewsReducer,
  questions: questionsReducer,
  players: playersReducer,
};