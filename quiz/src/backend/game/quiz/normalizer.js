import { Quiz, Question, DifficultyLevelBeginner, DifficultyLevelIntermediate, DifficultyLevelExpert } from "./quiz";

export const TypeOpenQuizzDB = 'openquizzdb';

export class Normalizer {

  constructor() {
    this._quizzes = {};
    this._normalizers= {
      [TypeOpenQuizzDB]: normalizeOpenQuizzDB,
    };
  }

  add(type, ...typedQuizzes) {
    const quizzes = this._quizzes[type] || [];
    quizzes.push(...typedQuizzes);
    this._quizzes[type] = quizzes;
  }

  exec() {
    return Object.keys(this._quizzes).reduce((quizzes, type) => {
      if (!this._normalizers[type]) throw new Error(`Unexpected quiz type '%s'`, type);
      if (!Array.isArray(this._quizzes[type])) return quizzes;
      
      const normalizeFn = this._normalizers[type];
      quizzes.push(...this._quizzes[type].map(normalizeFn));
      return quizzes;
    }, []);
  }

}

function normalizeOpenQuizzDB(rawQuiz) {
  const BeginnerLevelTag = "débutant";
  const IntermediateLevelTag = "confirmé";
  const ExpertLevelTag = "expert";
  const FrenchLocalTag = "fr";

  const author = rawQuiz.rédacteur;

  const translateDifficultyLevel = (rawDifficulty) => {
    switch(rawDifficulty) {
      case BeginnerLevelTag:
        return DifficultyLevelBeginner;
      case IntermediateLevelTag:
        return DifficultyLevelIntermediate;
      case ExpertLevelTag:
        return DifficultyLevelExpert;
      default:
        throw new Error(`Unexpected difficulty level "${rawDifficulty}"`)
    }
  };

  const transformQuestion = (difficulty, raw) => {
    return new Question(
      raw.question,
      raw.propositions,
      raw.propositions.indexOf(raw.réponse),
      difficulty,
      author,
      raw.anecdote
    );
  };

  const questions = Object.keys(rawQuiz.quizz).reduce((questions, key) => {
    switch(key) {
      case FrenchLocalTag:
        const frenchQuestions = rawQuiz.quizz[key][0];
        questions.push(...Object.keys(frenchQuestions).reduce((questions, rawDifficulty) => {
          const difficulty = translateDifficultyLevel(rawDifficulty);
          questions.push(...frenchQuestions[rawDifficulty].map(raw => transformQuestion(difficulty, raw)));
          return questions;
        }, []));
        break;
      case BeginnerLevelTag:
      case IntermediateLevelTag:
      case ExpertLevelTag:
        const difficulty = translateDifficultyLevel(key);
        questions.push(...rawQuiz.quizz[key].map(raw => transformQuestion(difficulty, raw)));
        break;
    }
    return questions;
  }, []);
  
  const theme = rawQuiz.thème.split("(", 1)[0].trim();

  return  new Quiz("OpenQuizzDB", theme, questions);
}