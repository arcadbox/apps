export class Quiz {
  constructor(source, theme, questions = []) {
    this._source = source;
    this._theme = theme;
    this._questions = questions;
  }

  getSource() {
    return this._source;
  }

  getTheme() {
    return this._theme;
  }

  getQuestions() {
    return this._questions;
  }

  getQuestion(questionIndex) {
    return this._questions[questionIndex];
  }

  getTotal() {
    return this._questions.length;
  }

  toJSON() {
    return {
      theme: this._theme,
      source: this._source,
      questions: this._questions
    };
  }
}

export const DifficultyLevelBeginner = 0;
export const DifficultyLevelIntermediate = 1;
export const DifficultyLevelExpert = 2;

export class Question {
  constructor(statement, answers, solutionIndex, difficultyLevel, author, trivia) {
    this._statement = statement;
    this._answers = answers;
    this._solutionIndex = solutionIndex;
    this._difficultyLevel = difficultyLevel;
    this._author = author;
    this._trivia = trivia;
  }

  getStatement() {
    return this._statement;
  }

  getAnswers() {
    return this._answers;
  }

  getSolutionIndex() {
    return this._solutionIndex;
  }

  getSolution() {
    return this.getAnswers()[this._solutionIndex];
  }

  getDifficultyLevel() {
    return this._difficultyLevel;
  }

  getAuthor() {
    return this._author;
  }

  getTrivia() {
    return this._trivia;
  }

  toJSON() {
    return {
      statement: this._statement,
      answers: this._answers,
      solutionIndex: this._solutionIndex,
      difficultyLevel: this._difficultyLevel,
      author: this._author,
      trivia: this._trivia,
    };
  }
}