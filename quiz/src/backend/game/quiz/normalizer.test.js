import { Normalizer, TypeOpenQuizzDB } from "./normalizer";

test("Normalize OpenQuizzDB", () => {

  const files = [
    '../resources/quizzes/openquizzdb/openquizzdb_4.json',
    '../resources/quizzes/openquizzdb/openquizzdb_24.json'
  ];

  const normalizer = new Normalizer();

  files.forEach(f => {
    const quiz = require(f);
    normalizer.add(TypeOpenQuizzDB, quiz);
  });

  const quizzes = normalizer.exec();

  expect(quizzes).not.toBeNull();
});