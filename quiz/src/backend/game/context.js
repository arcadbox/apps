export class Context {

  constructor({ players, quizzes, currentQuizIndex, currentQuestionIndex } = {}) {
    this._players = players || {};
    this._quizzes = quizzes || [];
    this._currentQuizIndex = currentQuizIndex || 0;
    this._currentQuestionIndex = currentQuestionIndex || -1;
    this._currentRound = -1;
  }

  currentRound() {
    return this._currentRound;
  }

  nextRound() {
    this._currentRound++;
  }

  currentQuestion() {
    return this.currentQuiz().getQuestion(this._currentQuestionIndex);
  }

  currentQuiz() {
    return this.getQuiz(this._currentQuizIndex);
  }

  getCurrentQuizIndex() {
    return this._currentQuizIndex;
  }

  setCurrentQuizIndex(quizIndex) {
    this._currentQuizIndex = quizIndex;
  }

  setCurrentQuestionIndex(questionIndex) {
    this._currentQuestionIndex = questionIndex;
  }

  getCurrentQuestionIndex() {
    return this._currentQuestionIndex;
  }

  getPlayer(playerId) {
    return this._players[playerId];
  }
  
  getPlayers() {
    return this._players;
  }

  isMultiplayer() {
    return Object.keys(this._players).length > 1;
  }

  setPlayerConnected(playerId, nickname) {
    this._players[playerId] = {
      nickname,
      answerIndex: -1,
      score: 0,
    };
  }

  setPlayerDisconnected(playerId) {
    delete this._players[playerId];
  }

  getQuiz(quizIndex) {
    return this._quizzes[quizIndex];
  }

  getQuizzes() {
    return this._quizzes;
  }

  setPlayerAnswer(playerId, answerIndex) {
    const currentQuestion = this.currentQuestion();
    const solutionIndex = currentQuestion.getSolutionIndex();
    
    const player = this._players[playerId];
    if (!player) return;

    player.answerTimestamp = Date.now();
    player.answerIndex = answerIndex;
    player.goodAnswer = solutionIndex === answerIndex;
  }

  resetPlayersAnswers() {
    console.log('resetting players answers');
    const players = this._players;
    Object.keys(players).forEach(playerId => {
      const p =  players[playerId];
      p.answerTimestamp = null;
      p.answerIndex = -1;
    });
  }

}