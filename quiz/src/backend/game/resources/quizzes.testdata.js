import { Normalizer, TypeOpenQuizzDB } from '../quiz/normalizer';

const files = [
  './quizzes/openquizzdb/openquizzdb_4.json',
  './quizzes/openquizzdb/openquizzdb_24.json'
];

const normalizer = new Normalizer();

files.forEach(f => {
  const quiz = require(f);
  normalizer.add(TypeOpenQuizzDB, quiz);
});

export default normalizer.exec();