# Questionnaires OpenQuizzDB

L'ensemble des questionnaires présents dans ce répertoire sont le fruit du travail du projet [OpenQuizzDB](http://openquizzdb.org/) et sont publiés sous licence [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.fr).
