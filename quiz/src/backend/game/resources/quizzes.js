import { importAll } from './webpack';
import { Normalizer, TypeOpenQuizzDB } from '../quiz/normalizer';

const openQuizzDBResources = require.context(
  './quizzes/openquizzdb', 
  false,
  /\.json$/
);
const openQuizzDBQuizzes = importAll(openQuizzDBResources);

const normalizer = new Normalizer();

normalizer.add(TypeOpenQuizzDB, ...openQuizzDBQuizzes);

export default normalizer.exec();