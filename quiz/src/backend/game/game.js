import { Context } from "./context";
import { Emitter } from "./emitter";

export const StateChangedEvent = 'stateChanged';

export class Game extends Emitter {

  constructor(context = new Context(), ...states) {
    super();
    this._context = context;
    this._states = states;
    this._currentState = null;
    this._proxyStateEvents();
  }

  nextState() {
    const currentState = this.getCurrentState();
    const context = this.getContext();

    for (let state, i = 0; (state = this._states[i]); ++i) {
      if (!state.isNext(context, currentState)) continue;
      
      if(currentState) currentState.leave(context, state);
      state.enter(context, currentState);

      this._currentState = state;

      this.emit(StateChangedEvent, {
        prevState: currentState,
        newState: state,
        delay: state.getDelay(),
        round: context.currentRound(),
      });

      return this._currentState;
    }

    throw new Error("Invalid game state !");
  }

  getCurrentState() {
    return this._currentState;
  }

  getContext() {
    return this._context;
  }

  _proxyStateEvents() {
    this._states.forEach(s => s.proxy(this));
  }

}