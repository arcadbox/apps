import { State } from "./state";
import { AnswerState } from "./answer-state";
import { LeaderboardState } from "./leaderboard-state";

export const QuestionChangedEvent = 'questionChanged';
export const ThemeChangedEvent = 'themeChanged';

export class QuestionState extends State {
  constructor(stateDelay = 11000, maxQuestionPerQuiz = 5) {
    super("QUESTION_STATE", stateDelay);
    this._config = {
      maxQuestionPerQuiz: maxQuestionPerQuiz,
    }
    this._internal = {
      coveredQuizQuestionIndexes: [],
    };
  }

  isNext(context, previousState) {
    if (!previousState) return true;
    if (previousState instanceof AnswerState)  return true;
    if (previousState instanceof LeaderboardState) return true;
  }

  enter(context, previousState) {
    context.nextRound();
    this.nextQuestion(context);
  }

  nextQuiz(context) {
    console.log('selecting next quiz...');
    this._internal.coveredQuizQuestionIndexes = [];
    let currentQuizIndex = context.getCurrentQuizIndex();
    currentQuizIndex = this.randomQuizzIndex(context, currentQuizIndex);
    context.setCurrentQuizIndex(currentQuizIndex);
    const currentQuiz = context.currentQuiz();
    this.emit(ThemeChangedEvent, { 
      theme: currentQuiz.getTheme(),
    });
  }

  nextQuestion(context) {
    console.log('selecting next question...');
    const nextQuestionIndex = this.nextRandomQuestionIndex(context);
    if (nextQuestionIndex === -1) {
      this.nextQuiz(context);
      return this.nextQuestion(context);
    }
    context.setCurrentQuestionIndex(nextQuestionIndex);

    const currentQuiz = context.currentQuiz();
    const currentQuestion = context.currentQuestion();

    this.emit(QuestionChangedEvent, { 
      statement: currentQuestion.getStatement(),
      answers: currentQuestion.getAnswers(),
      theme: currentQuiz.getTheme(),
      author: currentQuestion.getAuthor(),
      difficultyLevel: currentQuestion.getDifficultyLevel(),
    });
  }

  randomQuizzIndex(context, exludeIndex) {
    const totalQuizzes = context.getQuizzes().length;
    let randomIndex = Math.floor(Math.random() * totalQuizzes);
    if (randomIndex === exludeIndex) {
      randomIndex = (randomIndex+1) % totalQuizzes;
    }
    return randomIndex;
  }

  nextRandomQuestionIndex(context) {
    const coveredQuizQuestionIndexes = this._internal.coveredQuizQuestionIndexes;
    if (coveredQuizQuestionIndexes.length >= this._config.maxQuestionPerQuiz) return -1;

    const currentQuizIndex = context.getCurrentQuizIndex();
    const currentQuiz = context.getQuiz(currentQuizIndex);
    const totalQuestions = currentQuiz.getTotal();

    let randomIndex = Math.floor(Math.random() * totalQuestions);
    
    let counter = 0;
    while (coveredQuizQuestionIndexes.indexOf(randomIndex) !== -1) {
      randomIndex = (randomIndex+1) % totalQuestions;
      counter++;
      if (counter >= totalQuestions) return -1;
    }

    coveredQuizQuestionIndexes.push(randomIndex);
  
    return randomIndex;
  }


}