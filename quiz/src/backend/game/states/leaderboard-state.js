import { State } from "./state";
import { AnswerState } from "./answer-state";

export const LeaderboardChangedEvent = 'leaderboardChanged';

export class LeaderboardState extends State {
  constructor(stateDelay = 15000) {
    super("LEADERBOARD_STATE", stateDelay);
  }

  isNext(context, previousState) {
    const currentRound = context.currentRound();
    if (previousState instanceof AnswerState && currentRound % 5 === 0) return true;
  }

  enter(context) {
    this.emit(LeaderboardChangedEvent, { 
      leaderboard: this.getLeaderboard(context), 
    });
  }

  getLeaderboard(context) {
    const players = context.getPlayers();
    return Object.keys(players).map(playerId => {
      const { nickname, score } = players[playerId];
      return {
        id: playerId,
        nickname,
        score,
      };
    }).sort((p1, p2) => {
      if (p1.score < p2.score) return 1;
      if (p1.score > p2.score) return -1;
      return 0;
    });
  }

}