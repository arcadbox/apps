export class Emitter {

  constructor() {
    this._listeners = {};
    this._proxies = [];
  }

  _key(type) {
    return `_on${type}`;
  }

  on(type, cb) {
    const key = this._key(type);
    this._listeners[key] = this._listeners[key] || [];
    this._listeners[key].push(cb);
  }

  emit(type, ...args) {
    const key = this._key(type);
    this._listeners[key] && this._listeners[key].forEach((cb) => { cb(...args) });
    this._proxies.forEach(p => p.emit(type, ...args));
  }

  proxy(emitter) {
    this._proxies.push(emitter);
  }

}