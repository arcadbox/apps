import { Game } from "./game";
import { QuestionState } from "./states/question-state";
import { AnswerState } from "./states/answer-state";
import { Context } from "./context";
import quizzes from './resources/quizzes.testdata';

test('instanciate game', () => {
  const context = new Context({
    quizzes,
  });
  const states = [
    new QuestionState(),
    new AnswerState(),
  ];
  
  const game = new Game(context, ...states);

  for(let i = 0; i <= 10; i++) {
    let newState = game.nextState();
    console.log(newState);
    console.log(context);
  }

 

});