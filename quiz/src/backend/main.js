import quizzes from './game/resources/quizzes';
import { Context } from './game/context';
import { Game, StateChangedEvent } from './game/game';
import { QuestionState, ThemeChangedEvent, QuestionChangedEvent } from './game/states/question-state';
import { AnswerState, AnswerChangedEvent } from './game/states/answer-state';
import { LeaderboardState, LeaderboardChangedEvent } from './game/states/leaderboard-state';

const context = new Context({
  quizzes,
})
const game = new Game(
  context,
  new LeaderboardState(10000),
  new QuestionState(15000),
  new AnswerState(10000),
);

export function onInit() {
  rpc.register("selectAnswer");
  game.on(StateChangedEvent, onGameStateChanged);
  game.on(ThemeChangedEvent, onThemeChanged);
  game.on(QuestionChangedEvent, onQuestionChanged);
  game.on(AnswerChangedEvent, onAnswerChanged);
  game.on(LeaderboardChangedEvent, onLeaderboardChanged)
  nextGameState();
};

export function onUserConnect(playerId) {
  console.log(`Player connected: ${playerId}`);
  const { ID, Nickname } = user.getUserById(playerId);
  context.setPlayerConnected(playerId, Nickname);
  net.send(playerId, { 
    type: "PLAYER_INFO",  
    player: { 
      id: ID, 
      nickname: Nickname 
    }
  });
};

export function onUserDisconnect(playerId) {
  console.log(`Player disconnected: ${playerId}`);
  const player = context.getPlayer(playerId);
  if (player && player.score > 0) {
    highscore.add(playerId, player.score);
  }
  context.setPlayerDisconnected(playerId);
};

export function nextGameState() {
  console.log('next state');
  game.nextState();
}

export function selectAnswer(playerId, { selectedAnswerIndex }) {
  context.setPlayerAnswer(playerId, selectedAnswerIndex);
}

function onGameStateChanged({ prevState, newState, delay, round }) {
  net.broadcast({ 
    type: 'NEW_STATE',
    prevState: prevState ? prevState.getName() : null,
    newState: newState ? newState.getName() : null,
    delay,
    round,
  });
  console.log('delaying next game state...')
  timer.delay("nextGameState", delay);
}

function onThemeChanged(evt) {
  net.broadcast({ 
    type: 'NEW_THEME',
    ...evt,
  });
}

function onQuestionChanged(evt) {
  net.broadcast({ 
    type: 'NEW_QUESTION',
    ...evt,
  });
}

function onAnswerChanged(evt) {
  net.broadcast({ 
    type: 'NEW_ANSWER',
    ...evt,
  });
}

function onLeaderboardChanged(evt) {
  net.broadcast({ 
    type: 'NEW_LEADERBOARD',
    ...evt,
  });
}