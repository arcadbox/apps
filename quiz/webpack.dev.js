const webpack = require('webpack')
const common = require('./webpack.common.js')
const merge = require('webpack-merge')
const path = require('path')
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

module.exports = [
  merge(common[0], {
    devtool: 'source-map',
    mode: 'development',
    plugins: [
      new webpack.HotModuleReplacementPlugin(),
      new BundleAnalyzerPlugin()
    ],
    output: {
      path: path.join(__dirname, 'dist/public'),
      publicPath: './',
      filename: '[name].[hash].js'
    },
  }),
  merge(common[1], {
    mode: 'development',
    output: {
      path: path.join(__dirname, 'dist/backend'),
      publicPath: './',
      filename: 'main.js'
    }
  })
]