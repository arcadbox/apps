APPS := 2048 hextris api-test quiz virtual-trainer

.PHONY: bundle
bundle:
	rm -rf bundles
	$(MAKE) $(foreach g,$(APPS),bundle-$(g))

.PHONY: dist
dist:
	rm -rf dist
	$(MAKE) $(foreach g,$(APPS),dist-$(g))

dist-%:
	cd $* && make clean && make dist

bundle-%:
	mkdir -p bundles
	rm -f ../../bundles/$*.tar.gz
	$(MAKE) dist-$*
	cd $* && cd dist && tar -czf ../../bundles/$*.tar.gz *

clean:
	rm -rf bundles
	$(MAKE) $(foreach g,$(APPS),clean-$(g))

clean-%:
	cd $* && make clean