const webpack = require('webpack')
const HTMLPlugin = require('html-webpack-plugin')
const CopyPlugin = require('copy-webpack-plugin');


module.exports = [
  {
    entry: {
      main: ['babel-polyfill', './src/frontend/index.js'],
    },
    node: {
      fs: 'empty'
    },
    module: {
      rules: [
        { test: /\.(js|jsx)$/, exclude: /node_modules/, use: ['babel-loader'] },
        { 
          test: /\.(css)$/, 
          use: [
            'style-loader', 
            'css-loader',
            'sass-loader',
            {
              loader: 'postcss-loader',
              options: {
                ident: 'postcss',
                plugins: [
                  require('autoprefixer')(),
                ]
              }
            }
          ]
        },
        { test: /\.(png|jpg|jpeg|gif|svg|woff|woff2|ttf|eot)(\?.*$|$)/, use: ['file-loader'] },
        { test: /\.ya?ml$/, use: ['json-loader', 'yaml-loader'] }
      ]
    },
    resolve: {
      extensions: ['*', '.js', '.jsx']
    },
    plugins: [
      new webpack.ProvidePlugin({
        'React': 'react'
      }),
      new HTMLPlugin({
        title: 'MyApp',
        hash: true,
        template: 'src/frontend/index.html'
      }),
    ]
  },{
    entry: {
      main: ['babel-polyfill', './src/backend/index.js'],
    },
    node: {
      fs: 'empty'
    },
    module: {
      rules: [
        { test: /\.(js|jsx)$/, exclude: /node_modules/, use: ['babel-loader'] },
      ]
    },
    resolve: {
      extensions: ['*', '.js', '.jsx']
    },
    plugins: [
      new CopyPlugin([
        { from: 'src/arcad.yml', to: '../arcad.yml' }
      ])
    ]
  }
]