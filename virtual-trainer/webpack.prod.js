const common = require('./webpack.common.js')
const merge = require('webpack-merge')
const path = require('path')
const webpack = require('webpack')
const CleanWebpackPlugin = require('clean-webpack-plugin')

module.exports = [
  merge(common[0], {
    mode: 'production',
    output: {
      path: path.join(__dirname, 'dist/public'),
      publicPath: './',
      filename: '[name].[chunkhash].js'
    },
    devtool: 'source-map',
    plugins: [
      new CleanWebpackPlugin({dir: ['dist/public'] }),
      new webpack.DefinePlugin({
        'process.env.NODE_ENV': JSON.stringify('production')
      }),
    ]
  }),
  merge(common[1], {
    mode: 'production',
    output: {
      path: path.join(__dirname, 'dist/backend'),
      publicPath: './',
      filename: 'main.js'
    },
    plugins: [
      new CleanWebpackPlugin({dir: ['dist/backend'] }),
      new webpack.DefinePlugin({
        'process.env.NODE_ENV': JSON.stringify('production')
      }),
    ]
  })
]