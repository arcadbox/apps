export const BACKEND_PLAYER_INFO = 'BACKEND_PLAYER_INFO';
export const BACKEND_CHANNEL_CONNECT = 'BACKEND_CHANNEL_CONNECT';
export const BACKEND_CHANNEL_DISCONNECT = 'BACKEND_CHANNEL_DISCONNECT';

export function backendEvent(eventType, attrs) {
  return { type: `BACKEND_${eventType}`, ...attrs };
}