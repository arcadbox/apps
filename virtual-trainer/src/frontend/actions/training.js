export const USE_TRAINING = 'USE_TRAINING';

export function useTraining(training) {
  return { type: USE_TRAINING, training };
};