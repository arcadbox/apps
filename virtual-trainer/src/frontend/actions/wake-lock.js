export const LOCK_WAKE = 'LOCK_WAKE';

export function lockWake() {
  return { type: LOCK_WAKE };
};

export const UNLOCK_WAKE = 'UNLOCK_WAKE';

export function unlockWake() {
  return { type: UNLOCK_WAKE };
};