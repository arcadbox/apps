import exercises from '../resources/exercises';

export const TabataTraining = 'tabata';
export const TabataTotalExercises = 4;
export const TabataMinWorkDuration = 2;
export const TabataDefaultWorkDuration = 20;
export const TabataMinRounds = 1;
export const TabataDefaultRounds = 2;
export const TabataDefaultRestDuration = 10;
export const TabataMinRestDuration = 0;

export function generate({ selectedCategories, selectedBodyparts, selectedEquipments, rounds } = {}) {
  selectedCategories = selectedCategories || [];
  selectedBodyparts = selectedBodyparts || [];
  selectedEquipments = selectedEquipments || [];
  rounds = rounds || {},
  rounds = {
    workDuration: TabataDefaultWorkDuration,
    restDuration: TabataDefaultRestDuration,
    totalRounds: TabataDefaultRounds,
    ...rounds
  };
  
  console.log({selectedCategories, selectedBodyparts, selectedEquipments, rounds});
  const training = {
    type: TabataTraining,
    exercises: [],
    rounds: rounds,
    meta: {
      notEnoughExercises: false,
    }
  };

  const availableExercises = exercises.reduce((set, ex) => {
    console.log(`Filtering exercice`, ex);
    
    const matchesCategories = Array.isArray(ex.categories) && ex.categories.some(c => selectedCategories.includes(c))
    const matchesEquipments = Array.isArray(ex.equipments) && (ex.equipments.length === 0 || ex.equipments.some(e => selectedEquipments.includes(e)))
    const matchesBodyParts = Array.isArray(ex.bodyparts) && ex.bodyparts.some(b => selectedBodyparts.includes(b));

    console.log(`matchesCategories: ${matchesCategories}, matchesBodyParts: ${matchesBodyParts}, matchesEquipments: ${matchesEquipments}`);
    if (matchesCategories && matchesBodyParts && matchesEquipments) set.add(ex);
    return set;
  }, new Set());

  console.log("availableExercises", Array.from(availableExercises));

  if (availableExercises.size === 0) return training;

  for (let i = 0; i < TabataTotalExercises; i++) {
    let randomExercise;
    if (availableExercises.size > 0) {
      randomExercise = getRandomItem(availableExercises);
      availableExercises.delete(randomExercise);
    } else {
      // Reuse exercices if no more is available
      randomExercise = training.exercises[Math.floor(Math.random() * training.exercises.length)];
      training.meta.notEnoughExercises = true;
    }
    
    training.exercises.push(randomExercise);
  }

  return training;

};

function getRandomItem(set) {
  let items = Array.from(set);
  return items[Math.floor(Math.random() * items.length)];
}