import { importAll } from '../webpack';

const equipments = require.context(
  './', 
  false,
  /\.yml$/
);

export default importAll(equipments);