import { importAll } from '../webpack';

const categories = require.context(
  './', 
  false,
  /\.yml$/
);

export default importAll(categories);