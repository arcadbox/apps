import { importAll } from '../webpack';

const exercises = require.context(
  './', 
  false,
  /\.yml$/
);

export default importAll(exercises);