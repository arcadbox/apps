import { importAll } from '../webpack';

const resources = require.context(
  './', 
  false,
  /\.yml$/
);

export const BodyParts = importAll(resources);

export default BodyParts;

export function getRootBodyParts() {
  return BodyParts.filter(bp => !bp.parents || bp.parents.length === 0);
}

export function getSubBodyParts(parentName) {
  return BodyParts.filter(bp => bp.parents && bp.parents.includes(parentName));
}