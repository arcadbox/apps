import { HomePage } from "../pages/HomePage";
import { BootstrapPage as TabataBootstrapPage } from "../pages/Tabata/BootstrapPage";
import { TrainingPreviewPage } from "../pages/Tabata/TrainingPreviewPage";
import { TrainingPlayerPage } from "../pages/Tabata/TrainingPlayerPage";

let routeCounter = 0;
let pageCounter = 0;

export function newRoute(routeName, pageName, Component, withRouteUniqueId = false, withPageUniqueId = false) {
  return {
    id: getRouteId(routeName, withRouteUniqueId),
    render: (route, navigator) => <Component key={getPageKey(pageName, withPageUniqueId)} navigator={navigator} route={route} />
  }
}

export function newHomeRoute() {
  return newRoute("home", "home", HomePage);
}

export function newTabataBootstrapRoute() {
  return newRoute("tabata-bootstrap", "tabata-bootstrap", TabataBootstrapPage);
}

export function newTabataTrainingPreviewRoute() {
  return newRoute("tabata-training-preview", "tabata-training-preview", TrainingPreviewPage);
}

export function newTabataTrainingPlayerRoute() {
  return newRoute("tabata-training-player", "tabata-training-player", TrainingPlayerPage);
}

export function getRouteId(routeName, withUniqueId = false) {
  return `route-${routeName}${withUniqueId ? ('-' + routeCounter++) : ''}`
}

export function getPageKey(pageName, withUniqueId = false) {
  return `route-${pageName}${withUniqueId ? ('-' + pageCounter++) : ''}`
}