import { takeLatest, all } from "redux-saga/effects";
import { LOCK_WAKE, UNLOCK_WAKE } from "../actions/wake-lock";
import NoSleep from 'nosleep.js';

const noSleep = new NoSleep();

export function* wakeLockSaga() {
  yield all([
    takeLatest(LOCK_WAKE, lockWakeSaga),
    takeLatest(UNLOCK_WAKE, unlockWakeSaga)
  ]);
}

function* lockWakeSaga() {
  noSleep.enable();
}

function* unlockWakeSaga() {
  noSleep.disable();
}