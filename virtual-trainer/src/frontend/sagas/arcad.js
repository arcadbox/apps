import { eventChannel, END } from 'redux-saga';
import { call, take, put, takeLatest, all } from 'redux-saga/effects';
import { backendEvent } from '../actions/backend';

const Arcad = window.Arcad;

export function* arcadSaga() {
  yield all([
    handleArcadBackendEventSaga(),
  ]);
}

export function* handleArcadBackendEventSaga() {
  yield put({ type: 'BACKEND_CHANNEL_START' });
  let chan;
  try {
    chan = yield call(arcadChannel, "virtual-trainer");
    while (true) {
      let { type, ...attrs } = yield take(chan);
      yield put(backendEvent(type, attrs));
    }
  } catch(err) {
    if (chan) chan.close();
    yield put({ type: 'BACKEND_CHANNEL_ERROR', error: err });
  } finally {
    console.log('arcad connection lost')
  }
  yield put({ type: 'BACKEND_CHANNEL_STOP' });
}

function arcadChannel(gameId) {
  return eventChannel(emitter => {
      Arcad.connect(gameId)
        .catch(err => {
          emitter(err);
          emitter(END);
        });

      const onEvent = evt => {
        emitter(evt.detail);
      };
      const onConnect = () => {
        emitter({ type: 'CHANNEL_CONNECT' });
      };
      const onDisconnect = () => {
        emitter({ type: 'CHANNEL_DISCONNECT' });
      };
      const onError = (err) => {
        emitter(err);
      };

      Arcad.addEventListener('event', onEvent);
      Arcad.addEventListener('connect', onConnect);
      Arcad.addEventListener('disconnect', onDisconnect);
      Arcad.addEventListener('error', onError);

      return () => {
        Arcad.removeEventListener('event', onEvent);
        Arcad.removeEventListener('connect', onConnect);
        Arcad.removeEventListener('disconnect', onDisconnect);
        Arcad.removeEventListener('error', onError);
        Arcad.disconnect();
      }
    }
  )
}