import { all } from 'redux-saga/effects';
import { arcadSaga } from './arcad';
import { wakeLockSaga } from './wake-lock';

export default function* rootSaga() {
  yield all([
    arcadSaga(),
    wakeLockSaga(),
  ]);
}
