import React, { useState } from 'react';

const defaultColors = [ '#ff3860', '#ffdd57', '#3273dc', '#49c774' ];

export function ProgressRing({ radius = 60, stroke = 6, value = 0, max = 100, colors }) {
  colors = colors || defaultColors;

  const ratio = value / max;
  const normalizedRadius = radius - stroke * 2;
  const circumference = normalizedRadius * 2 * Math.PI;
  const strokeDashoffset = circumference - (ratio * circumference);

  let color;
  if (value < 0.25*max) {
    color = colors[0];
  } else if (value < 0.5*max) {
    color = colors[1];
  } else if (value < 0.75*max) {
    color = colors[2];
  } else {
    color = colors[3];
  }

  return (
    <svg className="progress-ring"
      height={radius * 2}
      width={radius * 2}>
      <circle
        stroke={color}
        strokeLinecap="round"
        fill="transparent"
        strokeWidth={ stroke }
        strokeDasharray={ `${circumference} ${circumference}` }
        style={ { strokeDashoffset } }
        strokeWidth={ stroke }
        r={ normalizedRadius }
        cx={ radius }
        cy={ radius }
      />
      <text x="50%" y="50%" 
        dominantBaseline="central" 
        textAnchor="middle">
        {max-value}
      </text>
    </svg>
  );
}