import React, { Fragment } from 'react';
import { 
  Icon, Range,
  ListItem, Input,
} from 'react-onsenui';
import { TabataMinWorkDuration, TabataMinRestDuration, TabataDefaultWorkDuration, TabataDefaultRestDuration, TabataDefaultRounds, TabataMinRounds } from '../../training/Tabata';

export function RoundsSelector({ values, onChange }) {
  const onValueChange = (name, evt) => {
    const value = parseInt(evt.target.value);
    if (Number.isNaN(value)) return;
    return onChange(name, value);
  }

  const onBlur = (name, evt) => {
    const value = parseInt(evt.target.value);
    
    if (Number.isNaN(value)) return;
    
    if (evt.target.max) {
      const max = parseInt(evt.target.max);
      if (value > max) return onChange(name, max);
    }

    if (evt.target.min) {
      const min = parseInt(evt.target.min);
      if (value < min) return onChange(name, min);
    }
  }

  return (
    <Fragment>
      <ListItem>
        <div className="left">
          <label htmlFor="workDuration">Temps de travail <br /><span style={{fontSize: '0.7rem'}}>(en secondes, min. {TabataMinWorkDuration})</span></label>
        </div>
        <div className="right">
          <Input type="number"
            onChange={onValueChange.bind(null, 'workDuration')}
            onBlur={onBlur.bind(null, 'workDuration')}
            inputId="workDuration"
            min={TabataMinWorkDuration}
            step={1}
            value={values.hasOwnProperty('workDuration') ? `${values.workDuration}` : `${TabataDefaultWorkDuration}`} />
        </div>
      </ListItem>
      <ListItem>
        <div className="left">
          <label htmlFor="restDuration">Temps de repos <br /><span style={{fontSize: '0.7rem'}}>(en secondes, min. {TabataMinRestDuration})</span></label>
        </div>
        <div className="right">
          <Input type="number"
            onChange={onValueChange.bind(null, 'restDuration')}
            onBlur={onBlur.bind(null, 'restDuration')}
            inputId="restDuration"
            min={TabataMinRestDuration}
            step={1}
            value={values.hasOwnProperty('restDuration') ? `${values.restDuration}` : `${TabataDefaultRestDuration}`} />
        </div>
      </ListItem>
      <ListItem>
        <div className="left">
          <label htmlFor="totalRounds">Nombre de tours <br /><span style={{fontSize: '0.7rem'}}>(min. {TabataMinRounds})</span></label>
        </div>
        <div className="right">
          <Input type="number"
            onChange={onValueChange.bind(null, 'totalRounds')}
            onBlur={onBlur.bind(null, 'totalRounds')}
            inputId={"totalRounds"}
            min={TabataMinRounds}
            step={1}
            value={values.hasOwnProperty('totalRounds') ? `${values.totalRounds}` : `${TabataDefaultRounds}`} />
        </div>
      </ListItem>
    </Fragment>
  )
}