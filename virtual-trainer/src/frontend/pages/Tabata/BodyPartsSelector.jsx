import React, { Fragment, useState } from 'react';
import { 
  Icon, 
  ListItem, Checkbox 
} from 'react-onsenui';
import { getRootBodyParts, getSubBodyParts } from '../../resources/bodyparts';

export function BodyPartsSelector({ values, onSelect }) {
  const roots = getRootBodyParts()
  return (
    <Fragment>
    {
      roots.map(bp => <BodyPart values={values} onSelect={onSelect} key={`bodypart-${bp.name}`} bp={bp} />)
    }
    </Fragment>
    )
}
  
export function BodyPart({ bp, values, onSelect }) {
  values = values || {};
  onSelect = onSelect ? onSelect : () => {};

  const subs = getSubBodyParts(bp.name);
  const expandable = subs.length > 0;
  const [ expanded, setExpanded ] = useState(false);

  const selectRootItem = evt => {
    const checked = !values[bp.name];
    onSelect(bp.name, checked);
    subs.forEach(sbp => onSelect(sbp.name, checked));
    evt.stopPropagation();
  };

  const selectItem = (bodypartName, evt) => {
    onSelect(bodypartName, !values[bodypartName]);
    evt.stopPropagation();
  }

  const toggleItem = () => {
    setExpanded(expanded => !expanded);
  };

  return (
    <Fragment>
      <ListItem
        tappable={true}
        expanded={expanded} 
        expandable={false}>
        <div className="left">
          <Checkbox
          checked={values[bp.name] ? values[bp.name] : false} 
          onChange={selectRootItem} />
        </div>
        <div className="center" onClick={selectRootItem}>{bp.label}</div>
        {
          expandable ?
          <div className="right" onClick={toggleItem}>
          {
            expanded ?
            <Icon icon="fa-chevron-up" /> :
            <Icon icon="fa-chevron-down" />
          }
          </div> :
          null
        }
      </ListItem>
      {
        expanded ?
        subs.map((sbp) => {
          return (
            <ListItem key={`bodypart-${bp.name}-${sbp.name}`}>
              <div className="left">&#8627;</div>
              <div className="center" onClick={selectItem.bind(null, sbp.name)}>
                <Checkbox
                  checked={values[sbp.name] ? values[sbp.name] : false} />
                <span style={{marginLeft:'10px'}}>{sbp.label}</span>
              </div>
              <div className="right" onClick={selectItem.bind(null, sbp.name)}></div>
            </ListItem>
            );
          }) :
          null
      }
    </Fragment>
  );
}