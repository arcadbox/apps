import React, { Fragment, useState, useEffect } from 'react';
import { useSelector, shallowEqual, useDispatch } from 'react-redux';
import { 
  Page, Toolbar, BottomToolbar, BackButton, Button, Icon } from 'react-onsenui';
import { ProgressRing } from '../../components/ProgressRing';
import { useOrientation } from '../../hooks/useOrientation';
import { unlockWake } from '../../actions/wake-lock';
import { speak } from '../../util/speak';

export function TrainingPlayerPage() {
  const training = useSelector(({ training }) => training.used, shallowEqual);
  const [, isLandscape ] = useOrientation();
  const dispatch = useDispatch();

  const [ state, setState ] = useState({
    paused: false,
    delay: 10,
    initialDelay: 10, 
    step: -1,
    round: 0,
    exerciseIndex: 0,
    isWork: false,
    isRest: false,
    isPreparation: true,
    done: false,
  });

  const exercise = training.exercises[state.exerciseIndex];
  const nextExercise = training.exercises[(state.exerciseIndex+1)%training.exercises.length];
  const hasPrevious = state.step > 0;
  const hasNext = state.step < (2 * training.exercises.length * training.rounds.totalRounds - 1);

  const computeState = (state, step) => {
    const delay = (step % 2 === 0 ? training.rounds.workDuration : training.rounds.restDuration);
    const initialDelay = delay;
    const round = Math.floor((step/2) / training.exercises.length);
    const exerciseIndex = Math.floor(step/2) % training.exercises.length;
    const isWork = step % 2 === 0;
    const isRest = !isWork;
    const done = round >= training.rounds.totalRounds;
    return {
      ...state,
      step,
      delay,
      initialDelay,
      round,
      exerciseIndex,
      isPreparation: false,
      isWork,
      isRest,
      done,
    };
  }

  const scheduleTick = () => {
    console.time('ticker');
    return setTimeout(() => {
      console.timeEnd('ticker');
      setState(state => {
        if (state.paused || state.done) return state;
        
        let delay = state.delay;
        delay--;
        
        // Speak
        if (delay <= 5 && delay > 0) speak(delay);
        if (delay === 0 && state.isWork) speak("Stop !");
        if (delay === 0 && (state.isRest || state.isPreparation)) speak("Go !");

        if (delay >= 0) return { ...state, delay };
        return computeState(state, state.step+1);
      });
    }, 1000);
  }

  const togglePause = () => setState(state => ({ ...state, paused: !state.paused }));
  const previousStep = () => {
    setState(state => {
      return computeState(state, state.step-1);
    }); 
  };
  const nextStep = () => {
    setState(state => {
      return computeState(state, state.step+1);
    });
  };

  useEffect(() => {
    const timeoutId = scheduleTick();
    return () => {
      clearTimeout(timeoutId);
    }
  });

  useEffect(() => {
    return () => dispatch(unlockWake());
  }, []);

  return (
    <Page
      renderToolbar={() => (
        <Toolbar>
          <div className="left">
            <BackButton />
          </div>
          <div className="center">Entrainement</div>
          <div className="right"></div>
        </Toolbar>
      )}
      renderBottomToolbar={() => (
        <BottomToolbar>
          <div style={{display:'flex', alignItems: 'center', justifyContent: 'center', height: '100%'}}>
            <Button modifier="quiet" ripple={true} disabled={!hasPrevious}
              onClick={previousStep}>
              <Icon icon="fa-step-backward" />
            </Button>
            <Button modifier="quiet" ripple={true} onClick={togglePause}>
              {
                state.paused ? 
                <Icon icon="fa-play" /> :
                <Icon icon="fa-pause" />
              }
            </Button>
            <Button modifier="quiet" ripple={true} disabled={!hasNext}
              onClick={nextStep}>
              <Icon icon="fa-step-forward" />
            </Button>
          </div>
        </BottomToolbar>
      )}
    >
      {
        state.done ?
        <Fragment>
          <div style={{
              textAlign:'center',
              display: 'flex',
              flexDirection: 'column',
              justifyContent: 'center',
              alignItems: 'center',
              height: '100%',
            }}>
            <h1>Session terminé !</h1>
            <h2><Icon icon="fa-angellist" style={{fontSize:'60px', fontWeight: 100, color: '#2878fc'}} /></h2>
          </div>
        </Fragment> :
        <Fragment>
          {
            isLandscape ?
            <LandscapeLayout {...state} exercise={exercise} 
              nextExercise={nextExercise} training={training} /> :
            <PortraitLayout {...state} exercise={exercise} 
              nextExercise={nextExercise} training={training} />
          }
        </Fragment>
      }      
    </Page>
  )
}

const WorkColors = [ '#ff3860', '#ffdd57', '#3273dc', '#49c774' ];
const RestColors = ([ ...WorkColors ]).reverse();

const portraitStyles = {
  container: {
    display:'flex', 
    flexDirection:'column', 
    justifyContent: 'center', 
    height: '100%'
  },
  progressContainer: {
    display:'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  infoContainer: {
    textAlign:'center'
  }
};

function PortraitLayout({ initialDelay, delay, isWork, round, training, isPreparation, isRest, exerciseIndex, exercise, nextExercise }) {
  return (
    <div style={portraitStyles.container}>
      <div style={portraitStyles.progressContainer}>
        <ProgressRing value={initialDelay-delay} max={initialDelay}
          colors={ isWork ? WorkColors : RestColors } /> 
      </div>
      <div style={portraitStyles.infoContainer}>
        <h2>Tour {round+1}/{training.rounds.totalRounds}</h2>
        <h1>
          { isPreparation ? "Préparez vous !" : null }
          { isWork ? exercise.label : null }
          { isRest ? "Repos" : null }
        </h1>
        {
          isWork ?
          <h4>Exercice {exerciseIndex+1}/{training.exercises.length}</h4> :
          null
        }
        {
          isRest || isPreparation ?
          <h4>Suivant: {nextExercise.label}</h4> :
          null
        }
      </div>
    </div>
  )
}

const landscapeStyles = {
  container: {
    display:'flex', 
    flexDirection:'row', 
    justifyContent: 'center', 
    height: '100%'
  },
  progressContainer: {
    display:'flex', 
    justifyContent: 'center', 
    alignItems: 'center',
    flex: '2',
  },
  infoContainer: {
    textAlign: 'center',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    flex: '3'
  },
};

function LandscapeLayout({ initialDelay, delay, isWork, round, training, isPreparation, isRest, exerciseIndex, exercise, nextExercise }) {
  return (
    <div style={landscapeStyles.container}>
      <div style={landscapeStyles.progressContainer}>
        <ProgressRing value={initialDelay-delay} max={initialDelay}
          colors={ isWork ? WorkColors : RestColors } /> 
      </div>
      <div style={landscapeStyles.infoContainer}>
        <h2>Tour {round+1}/{training.rounds.totalRounds}</h2>
        <h1>
          { isPreparation ? "Préparez vous !" : null }
          { isWork ? exercise.label : null }
          { isRest ? "Repos" : null }
        </h1>
        {
          isWork ?
          <h4>Exercice {exerciseIndex+1}/{training.exercises.length}</h4> :
          null
        }
        {
          isRest || isPreparation ?
          <h4>Suivant: {nextExercise.label}</h4> :
          null
        }
      </div>
    </div>
  )
}