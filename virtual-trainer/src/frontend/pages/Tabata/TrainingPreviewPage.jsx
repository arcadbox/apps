import React, { Fragment } from 'react';
import { useSelector, shallowEqual, useDispatch } from 'react-redux';
import { 
  Page, Toolbar, BottomToolbar, BackButton, Button, Icon, 
  List, ListItem, ListTitle, ListHeader, 
  Checkbox 
} from 'react-onsenui';
import * as moment from 'moment';
import { newTabataTrainingPlayerRoute } from '../../routes';
import { lockWake } from '../../actions/wake-lock';

export function TrainingPreviewPage({ navigator }) {
  const training = useSelector(({ training }) => training.used, shallowEqual);
  const dispatch = useDispatch();

  const startTraining = () => {
    dispatch(lockWake());
    navigator.pushPage(newTabataTrainingPlayerRoute());
  };

  const totalTime = training.rounds.totalRounds * (training.exercises.length * (training.rounds.workDuration + training.rounds.restDuration));
  const totalTimeStr = moment.duration(totalTime, 'seconds').humanize();
  return (
    <Page
      renderToolbar={() => (
        <Toolbar>
          <div className="left">
            <BackButton />
          </div>
          <div className="center">Prévisualisation</div>
          <div className="right"></div>
        </Toolbar>
      )}
      renderBottomToolbar={() => (
        <BottomToolbar>
          <Button style={{
              height:'100%', 
              borderRadius: 0, 
              display:'flex', 
              alignItems:'center',
              justifyContent: 'center'
            }}
            modifier="large--cta"
            onClick={startTraining}>
            Démarrer
            <Icon icon="fa-play" style={{marginLeft:'10px'}} />
          </Button>
        </BottomToolbar>
      )}
    >
      <ListTitle>Séquence proposée</ListTitle>
      {
        training.meta.notEnoughExercises ?
        <ListItem>
          <div className="center">
            Impossible de trouver assez d'exercices différents pour correspondre à vos critères. 
            Certains ont été réutilisés.
          </div>
        </ListItem> :
        null
      }
      <List>
        {
          training.exercises.map((ex, idx) => {
            return (
              <Fragment key={`round-item-${idx}`}>
                <ListItem>
                  <div className="left">
                    {ex.label}
                  </div>
                  <div className="right">{training.rounds.workDuration} secondes</div>
                </ListItem>
                {
                  training.rounds.restDuration !== 0 ?
                  <ListItem style={{backgroundColor: '#eceff1', fontSize: '0.8em'}}>
                    <div className="left" style={{minHeight: '28px'}}>Repos</div>
                    <div className="center" style={{minHeight: '28px'}}></div>
                    <div className="right" style={{minHeight: '28px'}}>{training.rounds.restDuration} secondes</div>
                  </ListItem> :
                  null
                }
              </Fragment>
            );
          })
        }
      </List>
      <ListTitle>
        <span>{training.rounds.totalRounds} tours</span> / <span>{totalTimeStr}</span>
      </ListTitle>
    </Page>
  )
}
