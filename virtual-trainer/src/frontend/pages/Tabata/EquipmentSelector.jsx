import React, { Fragment, useState } from 'react';
import { 
  Icon, 
  ListItem, Checkbox 
} from 'react-onsenui';
import equipments from '../../resources/equipments';

export function EquipmentsSelector({ values, onSelect }) {
  const selectItem = (equipmentName, evt) => {
    const checked = !values[equipmentName];
    onSelect(equipmentName, checked);
    evt.stopPropagation();
  };
  return (
    <Fragment>
    {
      equipments.map(eqmt => (
        <ListItem tappable={true} key={`equipment-${eqmt.name}`}>
          <div className="left">
            <Checkbox
              checked={values[eqmt.name] ? values[eqmt.name] : false} 
              onChange={selectItem.bind(null, eqmt.name)} />
          </div>
          <div className="center" onClick={selectItem.bind(null, eqmt.name)}>{eqmt.label}</div>
        </ListItem>
      ))
    }
    </Fragment>
  )
}