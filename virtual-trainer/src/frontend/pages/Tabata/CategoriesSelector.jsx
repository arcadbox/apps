import React, { Fragment, useState } from 'react';
import { 
  Icon, 
  ListItem, Checkbox 
} from 'react-onsenui';
import categories from '../../resources/categories';

export function CategoriesSelector({ values, onSelect }) {
  const selectItem = (categoryName, evt) => {
    const checked = !values[categoryName];
    onSelect(categoryName, checked);
    evt.stopPropagation();
  };
  return (
    <Fragment>
    {
      categories.map(cat => (
        <ListItem tappable={true} key={`category-${cat.name}`}>
          <div className="left">
            <Checkbox
              checked={values[cat.name] ? values[cat.name] : false} 
              onChange={selectItem.bind(null, cat.name)} />
          </div>
          <div className="center" onClick={selectItem.bind(null, cat.name)}>{cat.label}</div>
        </ListItem>
      ))
    }
    </Fragment>
  )
}