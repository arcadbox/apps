import React, { useState } from 'react';
import { 
  Page, Toolbar, BottomToolbar, BackButton, Button, Icon, 
  List, ListItem, ListTitle, ListHeader, Range,
  Checkbox 
} from 'react-onsenui';
import { useDispatch } from 'react-redux';
import { BodyPartsSelector } from './BodyPartsSelector';
import { CategoriesSelector } from './CategoriesSelector';
import { EquipmentsSelector } from './EquipmentSelector';
import { generate } from '../../training/Tabata';
import { useTraining } from '../../actions/training';
import { TrainingPreviewPage } from './TrainingPreviewPage';
import { RoundsSelector } from './RoundsSelector';
import { newTabataTrainingPreviewRoute } from '../../routes';

export function BootstrapPage({ navigator }) {
  const [ bodyparts, setBodyParts ] = useState({});
  const [ categories, setCategories ] = useState({});
  const [ equipments, setEquipments ] = useState({});
  const [ rounds, setRounds ] = useState({});

  const onSelectBodyPart = (name, selected) => {
    setBodyParts(bodyparts => ({ ...bodyparts, [name]: selected }));
  };

  const onSelectCategories = (name, selected) => {
    setCategories(categories => ({ ...categories, [name]: selected }));
  };

  const onSelectEquipments = (name, selected) => {
    setEquipments(equipments => ({ ...equipments, [name]: selected }));
  };

  const onChangeRounds = (name, value) => {
    console.log("round", name, value)
    setRounds(rounds => ({ ...rounds, [name]: value }));
  }

  const hasCategory = Object.values(categories).some(v => v);
  const hasBodyParts = Object.values(bodyparts).some(v => v);
  const canGenerate = hasCategory && hasBodyParts;

  const dispatch = useDispatch();

  const generateTraining = () => {
    const training = generate({
      selectedCategories: Object.keys(categories).filter(name => categories[name]),
      selectedBodyparts: Object.keys(bodyparts).filter(name => bodyparts[name]),
      selectedEquipments: Object.keys(equipments).filter(name => equipments[name]),
      rounds,
    });
    dispatch(useTraining(training));
    navigator.pushPage(newTabataTrainingPreviewRoute());
  };

  return (
    <Page
      renderToolbar={() => (
        <Toolbar>
          <div className="left">
            <BackButton />
          </div>
          <div className="center">Préparation de la session</div>
          <div className="right"></div>
        </Toolbar>
      )}
      renderBottomToolbar={() => (
        <BottomToolbar>
          <Button style={{
              height:'100%', 
              borderRadius: 0, 
              display:'flex', 
              alignItems:'center',
              justifyContent: 'center'
            }}
            disabled={!canGenerate}
            modifier="large--cta"
            onClick={generateTraining}>
            Générer une session
            <Icon icon="fa-bolt" style={{marginLeft:'10px'}} />
          </Button>
        </BottomToolbar>
      )}
    >
      <ListTitle>Paramètrage</ListTitle>
      <List>
        <ListHeader>Type d'exercice *</ListHeader>
        <CategoriesSelector values={categories} onSelect={onSelectCategories} />
        <ListHeader>Partie du corps à travailler *</ListHeader>
        <BodyPartsSelector values={bodyparts} onSelect={onSelectBodyPart} />
        <ListHeader>Équipement disponible</ListHeader>
        <EquipmentsSelector values={equipments} onSelect={onSelectEquipments} />
        <ListHeader>Personnalisation des tours</ListHeader>
        <RoundsSelector values={rounds} onChange={onChangeRounds} />
      </List>
    </Page>
  )
}
