import React, { useState } from 'react';
import { Page, Tabbar, Tab, Card, ListTitle, List, ListItem, Icon, Toolbar, BackButton } from 'react-onsenui';
import { BootstrapPage as TabataBootstrapPage } from './Tabata/BootstrapPage';
import { newTabataBootstrapRoute } from '../routes';

export function HomePage({ navigator }) {
  const [ tabIndex, setTabIndex ] = useState(0);
  return (
    <Page
      renderToolbar={() => (
        <Toolbar>
          <div className="center">Virtual Trainer</div>
          <div className="right"></div>
        </Toolbar>
      )}
    >
    <Tabbar
      onPreChange={({index}) => setTabIndex(index)}
      position='bottom'
      index={tabIndex}
      renderTabs={(activeIndex) => [
        {
          content: <LaunchTab key={"tab-launch"} active={activeIndex === 0} navigator={navigator} />,
          tab: <Tab key="launch-tab" label="Démarrer" icon="fa-heartbeat" />
        },
        {
          content:  <SettingsTab key={"tab-settings"} active={activeIndex === 1} />,
          tab: <Tab key="settings-tab" label="Settings" icon="fa-cogs" />
        }]
      }
    />
    </Page>
  )
}

export function LaunchTab({ navigator }) {
  const programs = [
    { label: "Tabata", icon: "fa-stopwatch" },
  ];
  const onTabataClick = () => {
    if (!navigator) return;
    navigator.pushPage(newTabataBootstrapRoute());
  };
  return (
    <Page>
      <ListTitle>Type de session</ListTitle>
      <List
        dataSource={programs}
        renderRow={(prg, idx) => {
          return (
            <ListItem
              tappable={true}
              onClick={onTabataClick}
              key={`answer-${idx}`}>
              <div className="left">
                <Icon icon={prg.icon} />
              </div>
              <div className="center">{prg.label}</div>
              <div className="right">
                <Icon icon="fa-chevron-right"/>
              </div>
            </ListItem>
          );
        }}
      />
    </Page>
  )
}

export function SettingsTab() {
  return (
    <Page>
      <Card>
        <p>Settings</p>
      </Card>
    </Page>
  )
}