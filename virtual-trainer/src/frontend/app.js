import React, { Fragment, useState, useEffect, useRef } from 'react';
import { Navigator } from 'react-onsenui';
import { newHomeRoute } from './routes';

export function App({}) {
  const navigatorRef = useRef();
  return (
    <Navigator
      ref={navigatorRef}
      renderPage={(route, navigator) => route.render(route, navigator)}
      initialRoute={newHomeRoute()}
    />
  );
}