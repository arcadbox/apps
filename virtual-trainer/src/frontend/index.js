import { hot } from 'react-hot-loader/root';
import ReactDOM from 'react-dom'
import { App } from './app'
import { configureStore } from './store/store'
import { Provider } from 'react-redux'

import 'onsenui/css/onsenui.css';
import 'onsenui/css/onsen-css-components.css';
import './css/base.css';
import './css/progress-ring.css';

const store = configureStore()

const HotApp = hot(App);

ReactDOM.render(
  <Provider store={store}>
    <HotApp />
  </Provider>,
  document.getElementById('app')
)
