const synth = window.speechSynthesis;

export function speak(message, opts = { voice: null, pitch: 1, rate: 1 }) {
  console.log("speaking", message);
  if (!synth) return;
  const utter = new SpeechSynthesisUtterance(message);
  utter.voice = opts.voice;
  utter.pitch = opts.pitch;
  utter.rate = opts.rate;
  synth.speak(utter);
};