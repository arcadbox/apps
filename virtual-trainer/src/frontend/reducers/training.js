import { USE_TRAINING } from "../actions/training";

const initialState = {
  used: null,
};

export function trainingReducer(state = initialState, action) {
  switch(action.type) {
    case USE_TRAINING:
      return handleUsedTraining(state, action);
  }
  return state;
}

function handleUsedTraining(state, action) {
  return {
    ...state,
    used: {
      ...action.training,
    }
  }
}
