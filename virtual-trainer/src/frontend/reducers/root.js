import { trainingReducer } from "./training";

export default {
  training: trainingReducer,
};