// import exercises from './game/resources/exercises';
import { Context } from './game/context';
import { Game, StateChangedEvent } from './game/game';

const context = new Context()
const game = new Game(
  context,
);

export function onInit() {
  // game.on(StateChangedEvent, onGameStateChanged);
  // game.on(ThemeChangedEvent, onThemeChanged);
  // game.on(QuestionChangedEvent, onQuestionChanged);
  // game.on(AnswerChangedEvent, onAnswerChanged);
  // game.on(LeaderboardChangedEvent, onLeaderboardChanged)
  // nextGameState();
};

export function onPlayerMessage(playerId) {}

export function onPlayerConnect(playerId) {
  console.log(`Player connected: ${playerId}`);
  const { ID, Nickname } = user.getUserById(playerId);
  context.setPlayerConnected(playerId, Nickname);
  net.send(playerId, { 
    type: "PLAYER_INFO",  
    player: { 
      id: ID, 
      nickname: Nickname 
    }
  });
};

export function onPlayerDisconnect(playerId) {
  console.log(`Player disconnected: ${playerId}`);
  const player = context.getPlayer(playerId);
  if (player && player.score > 0) {
    highscore.add(playerId, player.score);
  }
  context.setPlayerDisconnected(playerId);
};

export function nextGameState() {
  console.log('next state');
  game.nextState();
}

function onGameStateChanged({ prevState, newState, delay, round }) {
  const timeout = new Date(Date.now() + delay);
  net.broadcast({ 
    type: 'NEW_STATE',
    prevState: prevState ? prevState.getName() : null,
    newState: newState ? newState.getName() : null,
    delay,
    timeout,
    round,
  });
  console.log('delaying next game state...')
  timer.delay("nextGameState", delay);
}