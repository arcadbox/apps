import { State } from "./state";
import { QuestionState } from "./question-state";

export const AnswerChangedEvent = 'answerChanged';

export class AnswerState extends State {
  constructor(stateDelay = 10000) {
    super("ANSWER_STATE", stateDelay);
  }

  isNext(context, previousState) {
    if (previousState instanceof QuestionState) return true;
  }

  enter(context) {
    this.updateScores(context);

    const currentQuiz = context.currentQuiz();
    const currentQuestion = context.currentQuestion();

    this.emit(AnswerChangedEvent, { 
      statement: currentQuestion.getStatement(),
      results: this.getResults(context),
      solutionIndex: currentQuestion.getSolutionIndex(),
      answers: currentQuestion.getAnswers(),
      trivia: currentQuestion.getTrivia(),
      author: currentQuestion.getAuthor(),
      theme: currentQuiz.getTheme(),
    });
  }

  leave(context) {
    this.resetPlayersAnswers(context);
  }
  
  updateScores(context) {
    const winners = this.getSortedWinners(context);
    const players = context.getPlayers();
    const isMultiplayer = context.isMultiplayer();
    const solutionIndex = context.currentQuestion().getSolutionIndex();
    Object.keys(players).forEach(playerId => {
      const winnerIndex = winners.indexOf(playerId);
      const player =  players[playerId];
      if (winnerIndex === 0 && isMultiplayer) { // First player to send a good answer
        player.score += 20;
      } else if (winnerIndex !== -1) {
        player.score += 10; // Player had a good answer
      } else if (player.answerIndex !== -1 && player.answerIndex !== solutionIndex) {
        // Player had a bad answer
        player.score -= 5;
      }
      // Scores cannot be negative
      if (player.score < 0) player.score = 0;
    });
  }

  getResults(context) {
    const players = context.getPlayers();
    const winners = this.getSortedWinners(context);
    const isMultiplayer = context.isMultiplayer();
    return Object.keys(players).reduce((results, playerId) => {
      const { nickname, score } = players[playerId];
      const wasFirst = winners.length > 0 && winners[0] === playerId && isMultiplayer;
      results[playerId] = {
        wasFirst,
        nickname,
        score,
      }
      return results;
    }, {});
  }

  getSortedWinners(context) {
    const players = context.getPlayers();
    const winners = Object.keys(players).filter(playerId => players[playerId].goodAnswer).sort((p1, p2) => {
      if (players[p1].answerTimestamp < players[p2].answerTimestamp) return -1;
      if (players[p1].answerTimestamp > players[p2].answerTimestamp) return 1;
      return 0;
    });
    return winners;
  }

  resetPlayersAnswers(context) {
    console.log('resetting players answers');
    const players = context.getPlayers();
    Object.keys(players).forEach(playerId => {
      const p =  players[playerId];
      p.answerTimestamp = null;
      p.answerIndex = -1;
      p.goodAnswer = false;
    });
  }

}