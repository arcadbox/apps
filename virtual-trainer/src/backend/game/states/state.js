import { Emitter } from "../emitter";

export class State extends Emitter {

  constructor(name, delay = 10000) {
    super();
    this._name = name;
    this._delay = delay;
  }

  getDelay() {
    return this._delay;
  }

  getName() {
    return this._name;
  }

  isNext(context, previousState) {
    return false;
  }

  enter(context, previousState) {
    // Noop
  }

  leave(context, nextState) {
    // Noop
  }

  toString() {
    return this._name;
  }

}