export class Context {

  constructor({} = {}) {
    this._currentRound = 0;
  }

  currentRound() {
    return this._currentRound;
  }

  nextRound() {
    this._currentRound++;
  }

}