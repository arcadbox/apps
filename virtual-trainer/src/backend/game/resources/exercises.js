const exercises = require.context(
  './exercises', 
  false,
  /\.json$/
);

export default exercises;