import * as main from './main';

Object.keys(main).forEach(k => {
  global[k] = main[k];
});