const webpack = require('webpack')
const common = require('./webpack.common.js')
const merge = require('webpack-merge')
const path = require('path')

module.exports = [
  merge(common[0], {
    devtool: 'source-map',
    mode: 'development',
    plugins: [
      new webpack.HotModuleReplacementPlugin()
    ],
    output: {
      path: path.join(__dirname, 'dist/public'),
      publicPath: './',
      filename: '[name].[hash].js'
    },
    devServer: {
      contentBase: path.join(__dirname, 'dist/public'),
      compress: true,
      port: 9000,
      writeToDisk: true,
      hotOnly: true,
      hot: true,
    }
  }),
  merge(common[1], {
    mode: 'development',
    output: {
      path: path.join(__dirname, 'dist/backend'),
      publicPath: './',
      filename: 'main.js'
    }
  })
]