
// Called on backend initialization
function onInit() {
  console.log("backend started");

  // Register RPC exposed methods
  rpc.register("echo");
  rpc.register("throwError");
  rpc.register("addHighscore");
  rpc.register("listHighscores");

  rpc.register("add");
  rpc.register("reset");
  rpc.register("total");
}

// Called for each user connection
function onUserConnect(userID) {
  console.log("onUserConnect", userID)
}

// Called for each user disconnection
function onUserDisconnect(userID) {
  console.log("onUserDisconnect", userID)
}

// Called for each user message
function onUserMessage(userID, data) {
  console.log("onUserMessage", userID, data.now);
  net.send(userID, { now: data.now });
}

// Called for each file upload request
function onFileUpload(userId, fileId, fileInfo) {
  console.log("onFileUpload", userId, fileId, fileInfo);
  if (fileInfo.contentType !== "text/plain") return false;
  if (fileInfo.filename !== 'blob') return false;
  return true;
}

// Called for each file download request
function onFileDownload(userId, fileId) {
  console.log("onFileDownload", userId, fileId);
  return true;
}

// RPC methods

function echo(userID, params) {
  console.log("echoing", params);
  return params;
}

function throwError() {
  throw new Error("oh no !");
}

function addHighscore(userID, params) {
  console.log(userID, params);
  return highscore.add(userID, params.score);
}

function listHighscores() {
  return highscore.list();
}

var count = 0;

function add(userID, params) {
  console.log("add", userID, params);
  count += params.value;
  return count;
}

function reset() {
  count = 0;
}

function total() {
  return count;
}