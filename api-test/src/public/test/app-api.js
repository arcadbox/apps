Arcad.debug = true;

describe('Arcad', function() {

  describe('#connect()', function() {
    after(() => {
      Arcad.disconnect();
    });

    it('should open the connection', function() {
      return Arcad.connect("app.arcad.apitest")
        .then(() => {
          chai.assert.isNotNull(Arcad._conn);
        });
    });
  });

  describe('#disconnect()', function() {
    it('should close the connection', function() {
      Arcad.disconnect();
      chai.assert.isNull(Arcad._conn);
    });
  });

  describe('#send()', function() {
    before(() => {
      return Arcad.connect("app.arcad.apitest");
    });

    after(() => {
      Arcad.disconnect();
    });

    it('should send a message to the backend and echo back', function(done) {
      const now = new Date();
      const handler = evt => {
        chai.assert.equal(evt.detail.now, now.toJSON());
        Arcad.removeEventListener('event', handler);
        done();
      }

      // Backend should echo back message
      Arcad.addEventListener('event', handler);

      // Send message to backend
      Arcad.send({ now });
    });
  });
  
});

describe('Remote Procedure Call', function() {

  before(() => {
    return Arcad.connect("app.arcad.apitest");
  });

  after(() => {
    Arcad.disconnect();
  });

  it('should call the remote echo() method and resolve the returned value', function() {
    const foo = "bar";

    return Arcad.invoke('echo', { foo })
      .then(result => {
        chai.assert.equal(result.foo, foo);
      });
  });

  it('should call the remote throwError() method and reject with an error', function() {
    return Arcad.invoke('throwError')
      .catch(err => {
        // Assert that it's an "internal" error
        // See https://www.jsonrpc.org/specification#error_object
        chai.assert.equal(err.code, -32603);
      });
  });

  it('should call an unregistered method and reject with an error', function() {
    return Arcad.invoke('unregisteredMethod')
      .catch(err => {
        // Assert that it's an "method not found" error
        // See https://www.jsonrpc.org/specification#error_object
        chai.assert.equal(err.code, -32601);
      });
  });


  it('should call the add() method repetitively and keep count of the sent values', function() {
    this.timeout(5000);

    const values = [];
    for(let i = 0; i <= 1000; i++) {
      values.push((Math.random() * 1000 | 0));
    }
    return Arcad.invoke('reset')
      .then(() => {
        return Promise.all(values.map(v =>  Arcad.invoke("add", {value: v})));
      })
      .then(() => Arcad.invoke('total'))
      .then(remoteTotal => {
        const localTotal = values.reduce((t, v) => t+v);
        console.log("Remote total:", remoteTotal, "Local total:", localTotal);
        chai.assert.equal(remoteTotal, localTotal)
      })
  });

});

describe('Highscore Module', function() {

  before(() => {
    return Arcad.connect("app.arcad.apitest");
  });

  after(() => {
    Arcad.disconnect();
  });

  it('should add an highscore and return it', function() {
    // Generate a random score (integer)
    const score = Math.random() * 10000 | 0;
    return Arcad.invoke('addHighscore', {score})
      .then(result => {
        chai.assert.equal(result.Score, score);
      });
  });

  it('should list the highscore and return it', function() {
    return Arcad.invoke('listHighscores')
      .then(highscores => {
        chai.assert.isArray(highscores);
      });
  });

});

describe('File Module', function() {

  before(() => {
    return Arcad.connect("app.arcad.apitest");
  });

  after(() => {
    Arcad.disconnect();
  });

  it('should upload then download a file', function() {
    const content = "file "+(new Date()).toJSON();
    const file = new Blob([content], {type: "text/plain"});

    return Arcad.upload(file)
      .then(fileId => {

        chai.assert.isNotEmpty(fileId);
        
        const fileUrl = Arcad.fileUrl(fileId);
        chai.assert.isNotEmpty(fileUrl);

        return fetch(fileUrl)
          .then(res => res.text())
          .then(fileContent => {
            chai.assert.equal(content, fileContent);
          });
      });
  });

});
