function fetchPlayerHighscores() {
  return Arcad.invoke('findUserHighscores')
    .then(results => {
      highscores = results.map(r => r.Score);
      highscores.length = 'numHighScores' in window ? window.numHighScores : 3;
    });
}

function resetPlayerScore() {
  return Arcad.invoke('resetScore');
}

function updatePlayerScore(delta) {
  return Arcad.invoke('updateScore', { delta: delta });
}

function savePlayerScore(score) {
  if (score === 0) return Promise.resolve();
  return Arcad.invoke("saveScore", { score: score });
}