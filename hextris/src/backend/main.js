var scoreByPlayer = {};

function onInit() {
  rpc.register('updateScore');
  rpc.register('saveScore');
  rpc.register('resetScore');
  rpc.register('findUserHighscores');
}

function onUserConnect(userId) {
  scoreByPlayer[userId] = { lastUpdate: null, score: 0 };
}

function onUserDisconnect(userId) {
  delete scoreByPlayer[userId];
}

function updateScore(userId, params) {
  var currentScore = scoreByPlayer[userId];
  currentScore.lastUpdate = (new Date()).getTime();
  currentScore.score += params.delta;
  console.log("update score", userId, params, currentScore);
}

function saveScore(userId, params) {
  var currentScore = scoreByPlayer[userId];
  if (!currentScore) return;
  highscore.add(userId, currentScore.score);
  console.log("save score",userId, params, currentScore);
}

function resetScore(userId) {
  scoreByPlayer[userId] = { lastUpdate: null, score: 0 };
}

function findUserHighscores(userId) {
  return highscore.userList(userId);
}
